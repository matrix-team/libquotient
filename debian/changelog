libquotient (0.8.2-1) experimental; urgency=medium

  * New upstream release.
  * Build with Qt6.

 -- Hubert Chathi <uhoreg@debian.org>  Thu, 15 Aug 2024 20:25:43 -0400

libquotient (0.8.1.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Hubert Chathi <uhoreg@debian.org>  Mon, 13 May 2024 21:59:33 -0400

libquotient (0.8.1.2-1) experimental; urgency=medium

  * New upstream release.

 -- Hubert Chathi <uhoreg@debian.org>  Tue, 19 Mar 2024 17:16:07 -0400

libquotient (0.7.1-2) experimental; urgency=medium

  * debian/control: add dependency on libolm-dev libssl-dev for dev package
  * debian/rules: don't let test write in home directory

 -- Hubert Chathi <uhoreg@debian.org>  Wed, 11 Jan 2023 18:10:43 -0500

libquotient (0.7.1-1) experimental; urgency=medium

  * New upstream release.
  * Enable end-to-end encryption support.

 -- Hubert Chathi <uhoreg@debian.org>  Mon, 09 Jan 2023 17:31:27 -0500

libquotient (0.6.11-1) unstable; urgency=medium

  * New upstream release.

 -- Hubert Chathi <uhoreg@debian.org>  Sat, 16 Oct 2021 12:21:18 -0400

libquotient (0.6.6-1) unstable; urgency=high

  * New upstream release. This fixes a remotely-triggered crash (see
    https://github.com/quotient-im/libQuotient/issues/456).

 -- Andres Salomon <dilinger@debian.org>  Sat, 27 Mar 2021 23:51:17 -0400

libquotient (0.6.4-2) unstable; urgency=medium

  * Remove libqmatrixclient-dev dummy package; Hubert pointed out that
    we may want both dev packages installable, and they don't actually
    conflict.

 -- Andres Salomon <dilinger@debian.org>  Tue, 19 Jan 2021 17:21:30 -0500

libquotient (0.6.4-1) unstable; urgency=medium

  * New upstream release (closes: #980067).
  * Add myself as an uploader.
  * Upstream renamed the library from libqmatrixclient to libQuotient;
    rename the packages accordingly, with a dummy -dev package.

 -- Andres Salomon <dilinger@debian.org>  Mon, 18 Jan 2021 04:16:19 -0500

libqmatrixclient (0.5.3.2-1) unstable; urgency=medium

  * New upstream release.
  * Set packaging team as maintainer.

 -- Hubert Chathi <uhoreg@debian.org>  Fri, 17 Apr 2020 16:32:17 -0400

libqmatrixclient (0.5.2-1) unstable; urgency=medium

  * New upstream release.

 -- Hubert Chathi <uhoreg@debian.org>  Tue, 25 Jun 2019 16:35:11 -0400

libqmatrixclient (0.4.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Hubert Chathi <uhoreg@debian.org>  Mon, 14 Jan 2019 11:21:36 -0500

libqmatrixclient (0.4.1-1) unstable; urgency=medium

  * Initial release of splitting the library out from quaternion.

 -- Hubert Chathi <uhoreg@debian.org>  Mon, 24 Dec 2018 15:11:03 -0500
